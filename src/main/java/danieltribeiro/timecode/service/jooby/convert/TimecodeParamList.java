package danieltribeiro.timecode.service.jooby.convert;

import java.util.ArrayList;
import java.util.List;

import danieltribeiro.timecode.TimecodeParam;

public class TimecodeParamList {

  private List<String> list;

  public TimecodeParamList() {
    this.list = new ArrayList<>();
  }

  public TimecodeParamList(List<String> list) {
    this.list = list;
  }

  /**
   * @return the list
   */
  public List<String> getList() {
    return list;
  }

  /**
   * @param list the list to set
   */
  public void setList(List<String> list) {
    this.list = list;
  }

  public TimecodeParam asParam(int i) {
    return TimecodeParam.parse(list.get(i));
  }

}