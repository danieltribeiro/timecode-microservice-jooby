package danieltribeiro.timecode.service.jooby.convert;

import java.util.ArrayList;
import java.util.List;

import org.jooby.mvc.Body;
import org.jooby.mvc.GET;
import org.jooby.mvc.POST;
import org.jooby.mvc.Path;

import danieltribeiro.timecode.Timecode;

@Path("/convert")
public class ConvertController {

  @POST
  @Path("/:srcfr/:destfr/:offset")
  public List<Timecode> convertMultiple(String srcfr, String destfr, String offset, @Body List<String> list) throws Throwable {
    List<Timecode> newList = new ArrayList<>();
    for (String s : list) {
      newList.add(Timecode.parse(s, srcfr).convertFrameRate(destfr, offset));
    }
    return newList;
	}


  @GET
  @Path("/:srcfr/:value/:destfr/:offset")
  public Timecode convertWithOffset(String srcfr, String value, String destfr, String offset) throws Throwable {
    return Timecode.parse(value, srcfr).convertFrameRate(destfr, offset);
	}

  @GET
  @Path("/:srcfr/:value/:destfr")
  public Timecode convertWithoutOffset(String srcfr, String value, String destfr) throws Throwable {
    return convertWithOffset(srcfr, value, destfr, "0");
  }
}