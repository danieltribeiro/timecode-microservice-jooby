package danieltribeiro.timecode.service.jooby;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import danieltribeiro.timecode.Timecode;

public class TimecodeSerializer extends StdSerializer<Timecode> {
     
  public TimecodeSerializer() {
      this(null);
  }
 
  public TimecodeSerializer(Class<Timecode> t) {
    super(t);
  }

  @Override
  public void serialize(Timecode value, JsonGenerator gen, SerializerProvider provider) throws IOException {
  
    gen.writeStartObject();
    gen.writeStringField("string", value.toString());
    gen.writeNumberField("frameCount", value.getTotalFrames());
    gen.writeNumberField("milliseconds", value.getTotalMilis());
    gen.writeStringField("frameRate", value.getFramerate().getName());
    gen.writeEndObject();
  }

}